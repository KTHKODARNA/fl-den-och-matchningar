import java.util.*;


public class Bipmatch {
	Kattio io;
	int x, y, eBip, eFlow, start, utlopp;
	StringBuilder sb;

	public Bipmatch() {
		io = new Kattio(System.in, System.out);
		sb = new StringBuilder();
		reduceBipartiteGraph(sb);
	}

	void reduceBipartiteGraph(StringBuilder sb) {
		x = io.getInt();
		y = io.getInt();
		eBip = io.getInt(); // läs första raden
		
		start = x + y + 1;
		utlopp = x + y + 2; // start och utlopp (hörn)


		sb.append(x + y + 2 + "\n");
		sb.append(start + " " + utlopp + "\n");

		eFlow = eBip + x + y;

		sb.append(eFlow + "\n");

		for (int i = 0; i < eBip; i++) {
			int a = io.getInt();
			int b = io.getInt();
			sb.append(a + " " + b + " 1" + "\n"); // skriv ut alla kanter från matchingsgrafen
		}

		for(int i = 1; i <= x; i++) {
			sb.append(start + " " + i + " 1" + "\n"); // skriv ut kanter från start
		}

		for(int i = (x+1); i <= (x+y); i++) {
			sb.append(i + " " + utlopp + " 1" + "\n"); // skriv ut kanter till utlopp
		}
		
		System.err.println("Skickade iväg flödesgrafen");
	}



	public static void main(String args[]) {
		new Bipmatch();
	}

}