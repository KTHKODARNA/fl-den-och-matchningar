import java.util.*;

/**
 * Exempel på in- och utdatahantering för maxflödeslabben i kursen
 * ADK.
 *
 * Använder Kattio.java för in- och utläsning.
 * Se http://kattis.csc.kth.se/doc/javaio
 *
 * @author: Per Austrin
 */

public class BipRed {
	Kattio io;
	int x, y, eBip, eFlow, start, utlopp;

	int e = 0;


	/**
	 * Red.
	 */
	void reduceBipartiteGraph() {
		x = io.getInt();
		y = io.getInt();
		eBip = io.getInt(); // läs första raden
		
		start = x + y + 1;
		utlopp = x + y + 2; // start och utlopp (hörn)

		io.println(x + y + 2); // skriv ut första raden (antalet kanter) i flödesproblemet
		io.println(start + " " + utlopp); // skriv ut andra raden (start och utlopp) i flödesproblemet

		eFlow = eBip + x + y;

		io.println(eFlow); // skriv ut tredje raden i flödesproblemet

		for (int i = 0; i < eBip; i++) {
			int a = io.getInt();
			int b = io.getInt();
			io.println(a + " " + b + " 1"); // skriv ut alla kanter från matchingsgrafen
		}

		for(int i = 1; i <= x; i++) {
			io.println(start + " " + i + " 1"); // skriv ut kanter från start
		}

		for(int i = (x+1); i <= (x+y); i++) {
			io.println(i + " " + utlopp + " 1"); // skriv ut kanter till utlopp
		}

		io.flush();
		System.err.println("Skickade iväg flödesgrafen");

	}
	void readBipartiteGraph() {
	// Läs antal hörn och kanter


	// Läs in kanterna
		for (int i = 0; i < e; ++i) {
			int a = io.getInt();
			int b = io.getInt();
		}
	}

	void writeFlowGraph() {
		int v = 23, e = 0, s = 1, t = 2;

	// Skriv ut antal hörn och kanter samt källa och sänka
		io.println(v);
		io.println(s + " " + t);
		io.println(e);
		for (int i = 0; i < e; ++i) {
			int a = 1, b = 2, c = 17;
	    // Kant från a till b med kapacitet c
			io.println(a + " " + b + " " + c);
		}
	// Var noggrann med att flusha utdata när flödesgrafen skrivits ut!
		io.flush();

	// Debugutskrift
		System.err.println("Skickade iväg flödesgrafen");
	}

	void printBipSolution() {
		ArrayList<String> edges = new ArrayList<String>();

		int v = io.getInt();
		int s = io.getInt();
		int t = io.getInt();
		int totflow = io.getInt();
		int e = io.getInt();
		int eToPrint = e;

		io.println(x + " " + y);

		for(int i = 0; i < e; i++) {

			int a = io.getInt();
			int b = io.getInt();
			int f = io.getInt();
			if(a == s || b == t) {
				eToPrint--;
				continue;
			}

			edges.add(a + " " + b);
		}

		io.println(eToPrint);

		for(String str: edges) {
			io.println(str);
		}
		io.flush();
	}
	void readMaxFlowSolution() {
	// Läs in antal hörn, kanter, källa, sänka, och totalt flöde
	// (Antal hörn, källa och sänka borde vara samma som vi i grafen vi
	// skickade iväg)
		int v = io.getInt();
		int s = io.getInt();
		int t = io.getInt();
		int totflow = io.getInt();
		int e = io.getInt();

		for (int i = 0; i < e; ++i) {
	    // Flöde f från a till b
			int a = io.getInt();
			int b = io.getInt();
			int f = io.getInt();
		}
	}


	void writeBipMatchSolution() {
		int x = 17, y = 4711, maxMatch = 0;

	// Skriv ut antal hörn och storleken på matchningen
		io.println(x + " " + y);
		io.println(maxMatch);

		for (int i = 0; i < maxMatch; ++i) {
			int a = 5, b = 2323;
	    // Kant mellan a och b ingår i vår matchningslösning
			io.println(a + " " + b);
		}

	}

	BipRed() {
		io = new Kattio(System.in, System.out);

		reduceBipartiteGraph();
		printBipSolution();

		//readBipartiteGraph();

		//writeFlowGraph();

		//readMaxFlowSolution();

		//writeBipMatchSolution();

	// debugutskrift
		System.err.println("Bipred avslutar\n");

	// Kom ihåg att stänga ner Kattio-klassen
		io.close();
	}

	public static void main(String args[]) {
		new BipRed();
	}
}

