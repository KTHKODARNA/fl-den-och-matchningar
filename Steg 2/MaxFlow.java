import java.util.*;
import java.lang.Math;

public class MaxFlow {

    /**
     * Nu ska du skriva ett program som löser flödesproblemet. 
     * Programmet ska läsa indata från standard input och skriva lösningen till standard output. 
     * Se nedan hur in- och utdataformaten för flödesproblemet ser ut.
     */
    
    Kattio io;
    int[][] cMatrix, fMatrix;
    StringBuilder sb;
    ArrayList<Edge> edges = new ArrayList<Edge>();
    int[] path;
    int s,t,v;
    double total = 0;
    double ttt = 0;
    double dt = 0;
    Deque<Integer> q = new ArrayDeque<Integer>();
    NList[] nList;

    void solveFlow() {
    	v = io.getInt();
    	s = io.getInt();
    	t = io.getInt();
    	int e = io.getInt();
    	path = new int[v+1];

    	cMatrix = new int[v+1][v+1];
    	fMatrix = new int[v+1][v+1];
    	nList = new NList[v+1];

    	for(int i = 0; i < e; i++) {
    		int a = io.getInt();
    		int b = io.getInt();
    		int c = io.getInt();

    		cMatrix[a][b] = c;
    		edges.add(new Edge(a, b));
    		if(nList[a] == null) {
    			nList[a] = new NList();
    		}
    		if(nList[b] == null) {
    			nList[b] = new NList();
    		}
    		nList[a].add(b);
    		nList[b].add(a);
    	}
            //io.flush();

    	int maxFlow = 0;
    	int u_node, v_node;


    	while(breadthFirstSearch()) {
    		int pathFlow = Integer.MAX_VALUE;

    		for(v_node = t; v_node != s; v_node=path[v_node]) {
    			u_node = path[v_node];
    			pathFlow = Math.min(pathFlow, (cMatrix[u_node][v_node]-fMatrix[u_node][v_node]));

    		}


    		v_node = t;
    		while(v_node != s) {
    			u_node = path[v_node];
    			fMatrix[u_node][v_node] += pathFlow;
    			fMatrix[v_node][u_node] -= pathFlow;
    			v_node = u_node;
    		}

    		maxFlow += pathFlow;
    	}
    	io.println(v);
    	io.println(s + " " + t + " " + maxFlow);
    	int count = 0;

    	sb = new StringBuilder();

            /**for(int i = 1; i <= v; i++) {
                    for(int j = 1; j <= v; j++) {
                            if(fMatrix[i][j] < 1){
                                    continue;
                            }
                            count++;
                            sb.append(i + " " + j + " " + fMatrix[i][j] + "\n");
                    }
                }*/


                for(int i = 0; i < edges.size(); i++) {
                	Edge edge = edges.get(i);
                	if(fMatrix[edge.a][edge.b] < 1) {} else {
                		count++;
                		sb.append(edge.a + " " + edge.b + " " + fMatrix[edge.a][edge.b] + "\n");
                	}

                }
                io.println(count);
                io.println(sb.toString());

            }


            boolean breadthFirstSearch() {
            	boolean[] visited = new boolean[v+1];
            //Queue<Integer> q = new LinkedList<Integer>();
            	q.addLast(s);
            	visited[s] = true;

            	path[s] = -1;
               	//ttt = System.currentTimeMillis();
	
            	while(!q.isEmpty()) {
            		int u = q.pollFirst();


            		/*for(int i = 1; i <= v; i++) {*/
            		if(nList[u] == null) {
            			continue;
            		}
            		for(int i : nList[u].nList) {

            			if(visited[i] == false) {
            				if((cMatrix[u][i] - fMatrix[u][i]) > 0) {
            					path[i] = u;
            					visited[i] = true;
            					if(i != t) {
            						q.offerLast(i);
            					}
            				}
            			} 
            		}
            	}

           		//dt = System.currentTimeMillis();


           		total += (dt-ttt);

            	return (visited[t] == true);
            }

            MaxFlow () {
            	io = new Kattio(System.in, System.out);
            	//double t = System.currentTimeMillis();
            	solveFlow();
            	//double w = System.currentTimeMillis();
            	//io.println("TIME: " + (w-t));
            	//System.err.println("TAJM: " + total);
            	io.close();

            }


            public static void main(String args[]) {
            	new MaxFlow();
            }
        }