import java.util.*;


public class Bipmatch {
	Kattio io;
	MaxFlow flowSolver;
	int x, y;

	public Bipmatch() {
		io = new Kattio(System.in, System.out);
		flowSolver = new MaxFlow();
		reduceBipartiteGraph();
		io.flush();
		flowSolver.solveFlow();
		io.close();
	}

	void reduceBipartiteGraph() {
		x = io.getInt();
		y = io.getInt();
		int eBip = io.getInt(); // läs första raden
		
		int start = x + y + 1;
		int utlopp = x + y + 2; // start och utlopp (hörn)


		flowSolver.readV(x+y+2);
		flowSolver.readS(start);
		flowSolver.readT(utlopp);
		flowSolver.readXY(x, y);

		int eFlow = eBip + x + y;
		flowSolver.readE(eFlow);

		for (int i = 0; i < eBip; i++) {
			int a = io.getInt();
			int b = io.getInt();
			flowSolver.readEdge(a, b, 1);
		}

		for(int i = 1; i <= x; i++) {
			flowSolver.readEdge(start, i, 1);
		}

		for(int i = (x+1); i <= (x+y); i++) {
			flowSolver.readEdge(i, utlopp, 1);
		}
		
		System.err.println("Skapade flödesgrafen");
	}

	public static void main(String args[]) {
		new Bipmatch();
	}

}