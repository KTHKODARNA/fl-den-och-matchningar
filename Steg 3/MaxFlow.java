import java.util.*;
import java.lang.Math;

public class MaxFlow {

    /**
     * Nu ska du skriva ett program som löser flödesproblemet. 
     * Programmet ska läsa indata från standard input och skriva lösningen till standard output. 
     * Se nedan hur in- och utdataformaten för flödesproblemet ser ut.
     */
    Kattio io;
    //int[][] cMatrix, fMatrix;
    ArrayList<Edge> edges = new ArrayList<Edge>();
    int[] path;
    int s,t,v, e;
    int x, y;
    Deque<Integer> q = new ArrayDeque<Integer>();
    NList[] nList;
    Node u_Node;


    void readV(int v) {
        this.v = v;
        init();
    }

    void readS(int s) {
        this.s = s;
    }

    void readT(int t) {
        this.t = t;
    }

    void readE(int e) {
        this.e = e;
    }

    void readXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    void init() {
        path = new int[v+1];
        //cMatrix = new int[v+1][v+1];
        //fMatrix = new int[v+1][v+1];
        nList = new NList[v+1];
    }

    public void readEdge(int a, int b, int c) {
        //cMatrix[a][b] = c;
        edges.add(new Edge(a, b));
        if(nList[a] == null) {
            nList[a] = new NList();
        }
        if(nList[b] == null) {
            nList[b] = new NList();
        }
        nList[a].add(b, c);
        nList[b].add(a, 0);
    }

    void solveFlow() {

        int maxFlow = 0;
        int u_node, v_node;

        while(breadthFirstSearch()) {

            int pathFlow = Integer.MAX_VALUE;

            for(v_node = t; v_node != s; v_node=path[v_node]) {
                u_node = path[v_node];

                for(int n = 0; n < nList[u_node].nList.size(); n++) {
                    Node node = nList[u_node].nList.get(n);
                    if(node.b == v_node) {
                        u_Node = node;
                    }
                }
                //pathFlow = Math.min(pathFlow, (cMatrix[u_node][v_node]-fMatrix[u_node][v_node]));
                pathFlow = Math.min(pathFlow, u_Node.getCapacity() - u_Node.getFlow());
            }
            v_node = t;
            while(v_node != s) {
                u_node = path[v_node];

                for(int n = 0; n < nList[u_node].nList.size(); n++) {
                    Node node = nList[u_node].nList.get(n);
                    if(node.b == v_node) {
                        u_Node = node;
                    }
                }

                //fMatrix[u_node][v_node] += pathFlow;
                u_Node.setFlow(u_Node.getFlow() + pathFlow);

                for(int n = 0; n < nList[v_node].nList.size(); n++) {
                    Node node = nList[v_node].nList.get(n);
                    if(node.b == u_node) {
                        u_Node = node;
                    }
                }

                u_Node.setFlow(u_Node.getFlow() - pathFlow);
                v_node = u_node;
            }
            maxFlow += pathFlow;
        }
        int count = 0;

        for(int i = 0; i < edges.size(); i++) {

            Edge edge = edges.get(i);

            for(int n = 0; n < nList[edge.a].nList.size(); n++) {
                Node node = nList[edge.a].nList.get(n);
                if(node.b == edge.b) {
                    u_Node = node;
                }
            }

            if(u_Node.getFlow() < 1) {
                continue;
            } else if(edge.a == s || edge.b == t) {
               continue;
           }
           count++;
       }

        /*for(int i = 0; i < edges.size(); i++) {
            Edge edge = edges.get(i);
            if(fMatrix[edge.a][edge.b] < 1) {
                continue;
            } else if(edge.a == s || edge.b == t) {
               continue;
           }
           count++;
       }*/

       io.println(x + " " + y);
       io.println(count);

       for(int i = 0; i < edges.size(); i++) {

            Edge edge = edges.get(i);

            for(int n = 0; n < nList[edge.a].nList.size(); n++) {
                Node node = nList[edge.a].nList.get(n);
                if(node.b == edge.b) {
                    u_Node = node;
                }
            }

            if(u_Node.getFlow() < 1) {
                continue;
            } else if(edge.a == s || edge.b == t) {
               continue;
           }
           io.println(edge.a + " " + edge.b);
       }

       /*for(int i = 0; i < edges.size(); i++) {
        Edge edge = edges.get(i);
        if(fMatrix[edge.a][edge.b] < 1) {
           continue;
       } else if(edge.a == s || edge.b == t) {
           continue;
       }
       io.println(edge.a + " " + edge.b);
       io.flush();

   }*/


   io.flush();
}

boolean breadthFirstSearch() {
    boolean[] visited = new boolean[v+1];
    q.addLast(s);
    visited[s] = true;

    path[s] = -1;

    while(!q.isEmpty()) {
        int u = q.pollFirst();

        /*for(int i = 1; i <= v; i++) {*/
            if(nList[u] == null) {
                continue;
            }
            for(int n = 0; n < nList[u].nList.size(); n++) {
                Node node = nList[u].nList.get(n);
                int i = node.b;
                if(visited[i] == false) {
                    //if((cMatrix[u][i] - fMatrix[u][i]) > 0) {
                    if(node.getCapacity() - node.getFlow() > 0) {
                        path[i] = u;
                        visited[i] = true;
                        if(i != t) {
                            q.offerLast(i);
                        }
                    }
                } 
            }
        }

        return (visited[t] == true);
    }

    MaxFlow () {
        io = new Kattio(System.in, System.out);
    }

    public static void main(String args[]) {
        new MaxFlow();
    }
}