public class Node {
	int b;
	int capacity;
	int flow;

	public Node(int b, int c) {
		this.b = b;
		capacity = c;
		flow = 0;
	}

	public void setFlow(int f) {
		flow = f;
	}

	public int getCapacity() {
		return capacity;
	}

	public int getFlow() {
		return flow;
	}


}